<%@ include file="/views/include.jsp" %>

<html>
<head><title><spring:message code="inicio_titulo"/></title></head>
  <body>
    <h1><spring:message code="inicio_cabecera"/></h1>
    <p><fmt:message key="inicio_tiempo"/> <c:out value="${modelo.now}"/></p>
    <h3><fmt:message key="inicio_productos"/></h3>
    <c:forEach items="${modelo.productos}" var="iter">
     	 <c:out value="${iter.descripcion}"/> 
	 <i><c:out value="${iter.precio}"/></i> Euros<br><br>
    </c:forEach>
    
    <br>
    	<a href="<c:url value="iniciarprecio.action"/>">Incrementar Precios</a>
    <br>
   </body>
</html>