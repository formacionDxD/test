<%@ include file="/include.jsp" %>
<html>
<head>
<title><spring:message code="cambiarprecio_title"/></title>
<style> .error { color: red; }</style>
</head>
<body>
<h1><spring:message code="cambiarprecio_heading"/></h1>
<form:form method="post" modelAttribute="precioForm" action="cambiarprecio.action">
<table width="95%" bgcolor="f8f8ff" border="0" cellspacing="0" cellpadding="5">
<tr>
<td width="100px" align="left"><spring:message code="cambiarprecio_texto"/>
</td>
<td width="100px"><form:input path="porcentaje"/></td>
<td width="200px"><form:errors path="porcentaje" cssClass="error"/></td>
</tr>
</table>
<br/>
<input type="submit" align="center" value="Execute"/>
</form:form>
<a href="<c:url value="inicio.action"/>">Inicio</a>
</body>
</html>