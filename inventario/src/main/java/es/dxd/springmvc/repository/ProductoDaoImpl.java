package es.dxd.springmvc.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.dxd.springmvc.bean.Producto;

/**
 * Clase de accedo a datos para la tabla productos
 * 
 * @author DTUser
 *
 */
@Repository
public class ProductoDaoImpl implements ProductoDao {

	/** Serial Version	 */
	private static final long serialVersionUID = 1L;
	
	/** Entity Manager */
	private EntityManager em = null;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Producto> getProductos() {
		 return  em.createQuery("SELECT P FROM Producto P").getResultList();
	}

	@Override
	@Transactional
	public void saveProducto(Producto producto) {
        em.merge(producto);
	}

	/**
	 * Get Entity Manager
	 * @return
	 */
	public EntityManager getEm() {
		return em;
	}

	/**
	 * Set Entity Manager
	 * @param em
	 */
	@PersistenceContext
	public void setEm(EntityManager em) {
		this.em = em;
	}
}
