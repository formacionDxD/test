package es.dxd.springmvc.repository;

import java.io.Serializable;
import java.util.List;

import es.dxd.springmvc.bean.Producto;

/**
 * Interfaz que define el comportamiento de 
 * servicio de acceso a datos Producto
 * 
 * @author DTUser
 *
 */
public interface ProductoDao extends Serializable {

   /**
	* M�todo que nos devuelte una lista de productos
	*
	* @return
	*/
	public List<Producto> getProductos();

	/**
	 * M�todo para guardar un nuevo producto
	 * 
	 * @param producto
	 */
	public void saveProducto(Producto producto);
}
