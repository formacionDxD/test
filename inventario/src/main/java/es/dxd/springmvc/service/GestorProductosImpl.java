package es.dxd.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.dxd.springmvc.bean.Producto;
import es.dxd.springmvc.repository.ProductoDao;

/**
 * Servicio para la gesti�n de productos
 * 
 * @author DTUser
 *
 */
@Service
public class GestorProductosImpl implements GestorProductos {

	/** Serial Version  */
	private static final long serialVersionUID = 1L;
	
	/** Dao de Producto */
	@Autowired
    private ProductoDao productoDao;

	@Override
    public List<Producto> getListaProductos() {
        return productoDao.getProductos();
    }
	
	@Override
	public void incrementarPrecio(int porcentage) {
		List<Producto> listaProductos = productoDao.getProductos();
		if (listaProductos != null) {
		   for (Producto productoIter : listaProductos) {
			   double nuevoPrecio = productoIter.getPrecio().doubleValue() * (100 + porcentage)/100;
			   productoIter.setPrecio(nuevoPrecio);
			   productoDao.saveProducto(productoIter);
	       }
	   }  
	}
	
	/**
	 * Set Producto DAO
	 * 
	 * @param productDao
	 */
    public void setProductDao(ProductoDao productDao) {
        this.productoDao = productDao;
    }

}
