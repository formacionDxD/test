package es.dxd.springmvc.service;


import java.util.List;
import java.io.Serializable;

import es.dxd.springmvc.bean.Producto;

/**
 * Interface del Servicio GestorProdcutosImpl. Define el comportamiento
 * del servicio para la gesti�n de productos
 * 
 * @author DTUser
 *
 */
public interface GestorProductos extends Serializable {

	/**
	 * Metodo que incrementa el preciop
	 * 
	 * @param porcentaje
	 */
    public void incrementarPrecio(int percentage);
    
    /**
     * M�todo que permite recuperar los productos
     * 
     * @return
     */
    public List<Producto> getListaProductos();
    
}