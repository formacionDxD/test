package es.dxd.springmvc.web.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.dxd.springmvc.bean.Producto;

@Repository
public class ProductoDaoImpl implements ProductoDao {
	
private static final long serialVersionUID = 1L;
private EntityManager em = null;

@SuppressWarnings("unchecked")
@Override
@Transactional
public List<Producto> getProductos() {
	return em.createQuery("SELECT P FROM Producto P").getResultList();
}

@Override
@Transactional
public void saveProducto(Producto producto) {
em.merge(producto);
}

public EntityManager getEm() {
	return em;
	}
	@PersistenceContext
	public void setEm(EntityManager em) {
	this.em = em;
	}
}
