package es.dxd.springmvc.web;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.dxd.springmvc.bean.PrecioForm;
import es.dxd.springmvc.service.GestorProductos;

/**
 * Controllador de la vista cambiarprecio.jsp
 * 
 * @author DTUser
 *
 */
@Controller
public class PrecioController {

	/** Log4j */
	final Log logger = LogFactory.getLog(PrecioController.class);
	
	@Autowired
    private GestorProductos gestorProductos;

	/**
	 * M�todo para cambiar los precios de los productos
	 * 
	 * @param precioForm
	 * @param result
	 * @return
	 */
	@RequestMapping(value="/cambiarprecio.action", method=RequestMethod.POST)
    public String cambiarPrecio(@Valid PrecioForm precioForm, BindingResult result){

		// Si Error retornamos a la pagina que ya estamos.
		// BindingResult llevar� el error
		if (result.hasErrors()) {
            return "cambiarprecio";
        }
		
		// Obtenemos el porcentaje del formulario
        int porcentaje = precioForm.getPorcentaje();
        logger.debug("El precio se incrementa en " + porcentaje + "%.");
        gestorProductos.incrementarPrecio(porcentaje);

        // redirigimos a la p�gina de inicio
        return "redirect:/home.action";
    }

    /**
     * M�todo que inicia la pantalla de precios
     * 
     * @param request
     * @return
     * @throws ServletException
     */
    @RequestMapping(value="/iniciarprecio.action")
    protected ModelAndView iniciarPrecio(HttpServletRequest request) throws ServletException {
    	
    	// Cremos un nuevo Model and view
        PrecioForm precioForm = new PrecioForm();
        precioForm.setPorcentaje(15);
        return new ModelAndView("cambiarprecio", "precioForm", precioForm);
    }

    /**
     * 
     * @return
     */
	public GestorProductos getGestorProductos() {
		return gestorProductos;
	}

	/**
	 * 
	 * @param gestorProductos
	 */
	public void setGestorProductos(GestorProductos gestorProductos) {
		this.gestorProductos = gestorProductos;
	}
    
    
    
}
