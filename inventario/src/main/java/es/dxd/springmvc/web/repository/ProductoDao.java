package es.dxd.springmvc.web.repository;
import java.io.Serializable;
import java.util.List;
import es.dxd.springmvc.bean.Producto;
public interface ProductoDao extends Serializable {
public List<Producto> getProductos();
public void saveProducto(Producto producto);
}