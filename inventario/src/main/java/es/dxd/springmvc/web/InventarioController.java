package es.dxd.springmvc.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import es.dxd.springmvc.service.GestorProductos;

/**
 * Controlador principal de nuestro proyecto
 * 
 * @author DTUser
 *
 */
@Controller
public class InventarioController {
	
	final Log logger = LogFactory.getLog(InventarioController.class);

	@Autowired
	GestorProductos gestorProductos;
	
	/**
	 * M�todo que inicia la aplicaci�n
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
    @RequestMapping(value="/home.action")
    public ModelAndView iniciarInventario(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException{
        String now = (new Date()).toString();
        logger.debug("Returning inicio view with " + now);
        
        Map<String, Object> modelo = new HashMap<String, Object>();
        modelo.put("now", now);
        modelo.put("productos", this.gestorProductos.getListaProductos());

        return new ModelAndView("inicio", "modelo", modelo);
    }

    /**
     * 
     * @return
     */
	public GestorProductos getGestorProductos() {
		return gestorProductos;
	}

	/**
	 * 
	 * @param gestorProductos
	 */
	public void setGestorProductos(GestorProductos gestorProductos) {
		this.gestorProductos = gestorProductos;
	}
}
