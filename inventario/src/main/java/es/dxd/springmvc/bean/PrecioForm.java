package es.dxd.springmvc.bean;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Formulario de Precio
 * 
 * @author DTUser
 *
 */
public class PrecioForm implements Serializable {

	/** Serial Version  */
	private static final long serialVersionUID = 1L;
	
	/** Log4j */
	final Log logger = LogFactory.getLog(PrecioForm.class);
	
	@Min(0)
    @Max(50)
    private int porcentaje;

	/**
	 * Get del atributo de porcentaje
	 * 
	 * @return
	 */
	public int getPorcentaje() {
		return porcentaje;
	}

	/**
	 * Set Porcetanje
	 * 
	 * @param porcentaje
	 */
	public void setPorcentaje(int porcentaje) {
		logger.debug("Porcentaje set to " + porcentaje);
		this.porcentaje = porcentaje;
	}
	
	/**
	 * Sobreescribimos el m�todo toString
	 */
	public String toString(){
		return Integer.toString(this.porcentaje);
	}
}
