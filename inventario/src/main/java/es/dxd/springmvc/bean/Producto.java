package es.dxd.springmvc.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Clase que identifica a un producto del inventario
 * 
 * @author DTUser
 *
 */
@Entity
@Table(name="PRODUCTOS")
public class Producto implements Serializable {
	
	/** Serial Version */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
	Long identificador;
	
	/** Descripci�n del producto */
	@Column(name = "DESCRIPCION")
	String descripcion;
	
	/** Precio del producto */
	@Column(name = "PRECIO")
	Double precio;
	
	/**
	 * Constructor implicito
	 */
	public Producto(){
		
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * 
	 * @param descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * 
	 * @return
	 */
	public Double getPrecio() {
		return precio;
	}
	
	/**
	 * 
	 * @param precio
	 */
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	/**
	 * Sobreescribimos el m�todo toString
	 */
	public String toString(){
		return this.descripcion + " " + this.precio;
	}

	/**
	 * 
	 * @return
	 */
	public Long getIdentificador() {
		return identificador;
	}

	/**
	 * 
	 * @param identificador
	 */
	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}
}
