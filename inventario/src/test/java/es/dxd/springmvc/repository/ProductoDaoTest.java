package es.dxd.springmvc.repository;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.dxd.springmvc.bean.Producto;

/**
 * Test para verificar el correcto funcionamiento
 * del DAO Producto.
 * 
 * @author DTUser
 *
 */
public class ProductoDaoTest {

	/** Contexto de Aplicacion */
    private ApplicationContext context;
    
    /** DAO */
    private ProductoDao productoDao;

    @Before
    public void setUp() throws Exception {
        context = new ClassPathXmlApplicationContext("classpath:test-context.xml");
        productoDao = (ProductoDao) context.getBean("productoDaoImpl");
    }

    /**
     * Test para obtener toda la lista de productos
     */
    @Test
    public void testGetProductList() {
        List<Producto> products = productoDao.getProductos();
        assertEquals(products.size(), 3, 0);	   
    }

    /**
     * Tes para guarda productos modificados
     */
    @Test
    public void testSaveProduct() {
        List<Producto> products = productoDao.getProductos();

        Producto p = products.get(0);
        Double price = p.getPrecio();
        p.setPrecio(199.29D);
        productoDao.saveProducto(p);

        List<Producto> updatedProducts = productoDao.getProductos();
        Producto p2 = updatedProducts.get(0);
        assertEquals(p2.getPrecio(), 199.29, 0);

        p2.setPrecio(price);
        productoDao.saveProducto(p2);
    }
}