package es.dxd.springmvc.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.dxd.springmvc.bean.Producto;
import es.dxd.springmvc.repository.ProductoDao;

/**
 * Test para probar el servicio de gestor de productors
 * 
 * @author DTUser
 *
 */
public class GestorProductosTest {

	/** Contexto de Aplicacion */
    private ApplicationContext context;
	
	/** Gestor de productos */
    private GestorProductosImpl gestorProductos;
    
    /** Lista de Productos */
    private List<Producto> listaProductos;
    
    /** N�mero de productos */
    private static int CONTADOR_PRODUCTOS = 3;
    
    /** Producto 1 */
    private static Double PRECIO_SILLA = new Double(22.81);
    private static String DESCRIPCION_SILLA = "Silla";
    
    /** Producto 2*/
    private static Double PRECIO_TABLA = new Double(75.29);
    private static String DESCRIPCION_TABLA = "Mesa";
     
    /** Incremento de Precio % */
    private static int INCREMENTO_PRECIO_POSITIVO = 10; 
    
    /**
     * Configuraci�n del Test 
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    	
    	gestorProductos = new GestorProductosImpl();
    	context = new ClassPathXmlApplicationContext("classpath:test-context.xml");
        ProductoDao productoDao = (ProductoDao) context.getBean("productoDaoImpl");
        gestorProductos.setProductDao(productoDao);
   }

    /**
     * Test para verificar que no existen productos
     */
    @Test
    public void testProductosSinProductos() {
    	gestorProductos = new GestorProductosImpl();
        assertNull(gestorProductos.getListaProductos());
    }

    /** 
     * Test para verificar que exiten productos
     */
    @Test
    public void testGetProductos() {
        List<Producto> listaProductos = gestorProductos.getListaProductos();
        assertNotNull(listaProductos);        
        assertEquals(CONTADOR_PRODUCTOS, listaProductos.size());
    
        Producto producto = listaProductos.get(2);
        assertEquals(DESCRIPCION_SILLA, producto.getDescripcion());
        assertEquals(PRECIO_SILLA, producto.getPrecio());
        
        producto = listaProductos.get(1);
        assertEquals(DESCRIPCION_TABLA, producto.getDescripcion());
        assertEquals(PRECIO_TABLA, producto.getPrecio());      
    }
    
    /**
     * Test para verificar el correcto funcionamiento del m�todo
     * incrementar precio cuando la lista es null
     */
    @Test
    public void testIncrementarPrecioListaNull() {
        try {
        	gestorProductos = new GestorProductosImpl();
        	gestorProductos.incrementarPrecio(INCREMENTO_PRECIO_POSITIVO);
        }
        catch(NullPointerException ex) {
            fail("[ERROR] Lista de Productos Null");
        }
    }

    /**
     * Test para verificar el correcto funcionamiento del m�todo
     * incrementar precio cuando la lista es vacia
     */
    @Test
    public void testIncrementarPrecioListaVacia() {
        try {
        	gestorProductos = new GestorProductosImpl();
        	gestorProductos.incrementarPrecio(INCREMENTO_PRECIO_POSITIVO);
        }
        catch(Exception ex) {
        	fail("[ERROR] Lista de Productos vac�a");
        }           
    }
    
    /**
     * Test para verificar el correcto funcionamiento del m�todo
     * incrementar precio cuando la lista contiene productos
     */
    @Test
    public void testIncrementarPrecio() {
    	gestorProductos.incrementarPrecio(INCREMENTO_PRECIO_POSITIVO);
        double incrementoEsperadoSilla = 22.81;
        double incrementoEsperadoMesa = 75.29;
        
        List<Producto> listaProductos = gestorProductos.getListaProductos();      
        Producto producto = listaProductos.get(2);
        assertEquals(incrementoEsperadoSilla, producto.getPrecio(), 0);
        
        producto = listaProductos.get(1);      
        assertEquals(incrementoEsperadoMesa, producto.getPrecio(), 0);       
    }

}
