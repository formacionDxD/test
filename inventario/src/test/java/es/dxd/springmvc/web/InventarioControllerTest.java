package es.dxd.springmvc.web;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import es.dxd.springmvc.repository.ProductoDao;
import es.dxd.springmvc.service.GestorProductosImpl;
import es.dxd.springmvc.web.InventarioController;

/**
 * JUnit para probar el controlador del inventeario
 * 
 * @author DTUser
 *
 */
public class InventarioControllerTest {

	/** Contexto de Aplicacion */
    private ApplicationContext context;
    
    /** DAO */
    private ProductoDao productoDao;

    @Before
    public void setUp() throws Exception {
        context = new ClassPathXmlApplicationContext("classpath:test-context.xml");
        productoDao = (ProductoDao) context.getBean("productoDaoImpl");
    }
	
	@Test
    public void testIniciarInventario() throws Exception{		
        InventarioController controller = new InventarioController();
        GestorProductosImpl gestorProductosImpl = new GestorProductosImpl();
        gestorProductosImpl.setProductDao(productoDao);
        controller.setGestorProductos(gestorProductosImpl);
        ModelAndView modelAndView = controller.iniciarInventario(null, null);		
        assertEquals("inicio", modelAndView.getViewName());
        assertNotNull(modelAndView.getModel());
        
        @SuppressWarnings("unchecked")
		Map<String, Object> modelMap = (Map<String, Object>) modelAndView.getModel().get("modelo");
        
        String nowValue = (String) modelMap.get("now");
        assertNotNull(nowValue);
    }

}
