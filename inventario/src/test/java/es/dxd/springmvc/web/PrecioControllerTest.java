package es.dxd.springmvc.web;


import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;

import es.dxd.springmvc.bean.PrecioForm;
import es.dxd.springmvc.service.GestorProductosImpl;

/**
 * JUnit para probar el controlador del cambio de precio
 * 
 * @author DTUser
 *
 */
public class PrecioControllerTest {

	/** Precio Form */
	PrecioForm precioForm = null;
	
	@Before
	public void setUp() throws Exception {
		precioForm = new PrecioForm();
	}
	
	@Test
    public void testIniciarPrecioController() throws Exception{		
        
		PrecioController controller = new PrecioController();
        controller.setGestorProductos(new GestorProductosImpl());
        ModelAndView modelAndView = controller.iniciarPrecio(null);		
        assertEquals("cambiarprecio", modelAndView.getViewName());
        assertNotNull(modelAndView.getModel());
        
		PrecioForm precioForm = (PrecioForm) modelAndView.getModel().get("precioForm");
        assertNotNull(precioForm);
    }

	/**
	 * Test Precio correcto
	 * 
	 * @throws Exception
	 */
	@Test
    public void testIncrementarPrecioCorrecto() throws Exception{		
        
		PrecioController controller = new PrecioController();
        controller.setGestorProductos(new GestorProductosImpl());
        precioForm.setPorcentaje(15);
        
        BindingResult bindingResult = new BeanPropertyBindingResult(precioForm, "precioForm");
        String pagina = controller.cambiarPrecio(precioForm, bindingResult);		
        assertEquals("redirect:/home.action", pagina);
    }
	
	
	/**
	 * Test Precio incorrecto
	 *  
	 * @throws Exception
	 */
	@Test
    public void testIncrementarPrecioIncorrecto() throws Exception{		
        
		PrecioController controller = new PrecioController();
        controller.setGestorProductos(new GestorProductosImpl());
        precioForm.setPorcentaje(152);
        
        BindingResult bindingResult = new BeanPropertyBindingResult(precioForm, "precioForm");
        bindingResult.addError(new ObjectError("error", "error de validacion"));
        String pagina = controller.cambiarPrecio(precioForm, bindingResult);		
        assertEquals("cambiarprecio", pagina);
    }
}
