package es.dxd.springmvc.bean;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Test para la clase producto
 * 
 * @author DTUser
 *
 */
public class ProductoTest {

	/** Producto */
	private Producto producto;

	/**
	 * Configuración inicial
	 * 
	 * @throws Exception
	 */
    @Before
    public void setUp() throws Exception {
        producto = new Producto();
    }
    
    
    /**
     * Test para verificar el set y el get
     * del atributo descripción
     */
    @Test
    public void testSetAndGetDescripcion() {
        String testDescripcion = "descripción";
        assertNull(producto.getDescripcion());
        producto.setDescripcion(testDescripcion);
        assertEquals(testDescripcion, producto.getDescripcion());
    }

    /**
     * Test para verificar el set y el get del atributo
     * price
     */
    @Test
    public void testSetAndGetPrice() {
        double testPrecio = 100.00;
        producto.setPrecio(testPrecio);
        assertEquals(testPrecio, producto.getPrecio(), 0);
    }

}
