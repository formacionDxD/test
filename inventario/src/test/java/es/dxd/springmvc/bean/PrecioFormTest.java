package es.dxd.springmvc.bean;


import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;

/**
 * Test para validar el precio de formulario
 * 
 * @author DTUser
 *
 */
public class PrecioFormTest {

	/** Formulario de Precio */
	private PrecioForm precioForm;
	
	/** Constantes Test */
	public final int PORCENTAJE_OK = 25;
	public final int PORCENTAJE_KO_50 = 51;
	public final int PORCENTAJE_KO_NEG = -1;
	
	/** Validador */
	public static Validator validator; 

	/**
	 * Configuración inicial
	 * 
	 * @throws Exception
	 */
    @Before
    public void setUp() throws Exception {
       ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
       validator = factory.getValidator();
       precioForm = new PrecioForm();
    }
    
    
    /**
     * Test para verificar el correcto funcionamiento del porcentaje
     */
    @Test
    public void testSetPorcentajeOK() {
    	precioForm.setPorcentaje(this.PORCENTAJE_OK);
    	Set<ConstraintViolation<PrecioForm>> constraintViolations = validator.validate(precioForm);
        assertEquals(PORCENTAJE_OK, precioForm.getPorcentaje());
        assertEquals(0, constraintViolations.size());
    }
    
    /**
     * Test para verificar el correto funcionamiento del porcentajes > 50
     */
    @Test
    public void testSetPorcentajeKO_51() {
    	precioForm.setPorcentaje(PORCENTAJE_KO_50);
    	Set<ConstraintViolation<PrecioForm>> constraintViolations = validator.validate(precioForm);
        assertEquals(PORCENTAJE_KO_50, precioForm.getPorcentaje());
        assertEquals(1, constraintViolations.size());
        assertEquals("must be less than or equal to 50",constraintViolations.iterator().next().getMessage());
    }

    /**
     * Test para verificar el correto funcionamiento del porcentajes < 0
     */
    @Test
    public void testSetPorcentajeKO_NEG() {
    	precioForm.setPorcentaje(PORCENTAJE_KO_NEG);
    	Set<ConstraintViolation<PrecioForm>> constraintViolations = validator.validate(precioForm);
    	assertEquals(PORCENTAJE_KO_NEG, precioForm.getPorcentaje());
        assertEquals(1, constraintViolations.size());
        assertEquals("must be greater than or equal to 0",constraintViolations.iterator().next().getMessage());
    }

}
